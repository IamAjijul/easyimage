package com.scansione.easyimage.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import scansione.mindvalley_ajijulmondal_android_test.R;
import scansione.mindvalley_ajijulmondal_android_test.adapter.LandingRecyclerAdapter;
import scansione.mindvalley_ajijulmondal_android_test.gateway.NetworkRequest;
import scansione.mindvalley_ajijulmondal_android_test.helper.Constant;
import scansione.mindvalley_ajijulmondal_android_test.interfaces.SetOnItemClickListener;
import scansione.mindvalley_ajijulmondal_android_test.model_classes.BaseModel;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class LandingActivity extends BaseActivity implements SetOnItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "LandingActivity";
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager staggeredGridLayoutManager = null;
    private ArrayList<BaseModel> myList;
    private LandingRecyclerAdapter adapter = null;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        initMyClassViewsAndVariables();
    }

    /**
     * This function used to initialized all class view and variable
     */
    private void initMyClassViewsAndVariables() {
        recyclerView = (RecyclerView) findViewById(R.id.landingActivity_recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.landingActivity_swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor("#FFFA8303"));
        swipeRefreshLayout.setOnRefreshListener(this);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        myList = new ArrayList<>();
        adapter = new LandingRecyclerAdapter(myList, this, this);
        recyclerView.setAdapter(adapter);
        fetchDataFromServer();
    }

    private void fetchDataFromServer() {
        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqWithLoader(this, "http://pastebin.com/raw/wgkJgazE", TAG, new NetworkRequest.NetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    if (swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);

                    JSONArray jsonArray = new JSONArray(response);
                    Type listType = new TypeToken<List<BaseModel>>() {
                    }.getType();
                    Gson gson = new Gson();
                    myList = gson.fromJson(jsonArray.toString(), listType);

                    adapter.notifyMyAdapter(myList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    @Override
    public void onItemClick(int position, ImageView imageView) {
        Intent intent = new Intent(LandingActivity.this, DetailsActivity.class);
        intent.putExtra(Constant.EXTRA, myList.get(position));
        Pair<View, String> p1 = Pair.create((View) imageView, getString(R.string.transition_name));
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, p1);
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onRefresh() {
        fetchDataFromServer();
    }
}
