package com.scansione.easyimage.logger;

import android.support.v4.BuildConfig;
import android.util.Log;


/**
 * Created by AJIJUL on 12/3/2016.
 */
public class Logger {

    private static String logTag = "LOG";
    private static String extraCheractor = "";

    public static final void setLogTag(String tag) {
        if (BuildConfig.DEBUG) Logger.logTag = tag;
    }

    public static final void setExtraChar(String spcl) {
        if (BuildConfig.DEBUG) Logger.extraCheractor = spcl;
    }

    public static final void showErrorLog(String msg) {
        if (BuildConfig.DEBUG) Log.e(logTag, extraCheractor + msg);
    }

    public static final void showVerboseLog(String msg) {
        if (BuildConfig.DEBUG) Log.v(logTag, extraCheractor + msg);
    }

    public static final void showVerboseLog(String tag, String msg) {
        if (BuildConfig.DEBUG){
            Log.v(tag, extraCheractor + msg);
        }
    }

    public static final void showDebugLog(String msg) {
        if (BuildConfig.DEBUG) Log.d(logTag, extraCheractor + msg);
    }

    public static final void showDebugLog(String tag, String msg) {
        if (BuildConfig.DEBUG){
            Log.d(tag, extraCheractor + msg);
        }
    }

    public static final void showWarningLog(String msg) {
        if (BuildConfig.DEBUG) Log.w(logTag, extraCheractor + msg);
    }

    public static final void showInfoLog(String msg) {
        if (BuildConfig.DEBUG) Log.i(logTag, extraCheractor + msg);
    }

    public static final void showInfoLog(String tag, String msg) {
        if (BuildConfig.DEBUG){
            Log.i(tag, extraCheractor + msg);
        }
    }

    public static final void showWTFLog(String msg) {
        if (BuildConfig.DEBUG) Log.wtf(logTag, extraCheractor + msg);
    }

    public static final void print(String msg) {
        if (BuildConfig.DEBUG) System.out.println(extraCheractor + msg);
    }

    public static final void printStackTrace(Exception e) {
        if (BuildConfig.DEBUG) e.printStackTrace();
    }
}
