package com.scansione.easyimage.gateway;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

import scansione.mindvalley_ajijulmondal_android_test.R;
import scansione.mindvalley_ajijulmondal_android_test.logger.Logger;

/**
 * Created by AJIJUL on 12/3/2016.
 */
public class NetworkRequest {
    private static NetworkRequest ourInstance;
    private static Context mContext;

    public static NetworkRequest getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new NetworkRequest(context);
        }
        return ourInstance;
    }

    private NetworkRequest(Context context) {
        this.mContext = context;
    }

    public void strReq(final String url, final String tag, final NetworkResponse responseCallback) {
        StringRequest volleyStrReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
            }
        });

        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReq(final String url, final String tag, final Map<String, String> map,
                       final NetworkResponse responseCallback) {
        StringRequest volleyStrReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReqWithLoader(Context _context, final String url, final String tag, final NetworkResponse responseCallback) {
        final ProgressDialog mProgressDialog = PD(_context);
        StringRequest volleyStrReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        });
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReqWithLoader(Context _context, final String url, final String tag, final Map<String, String> map,
                                 final NetworkResponse responseCallback) {
        final ProgressDialog mProgressDialog = PD(_context);
        StringRequest volleyStrReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "NetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    // Loader with without Loading text
    private ProgressDialog PD(Context pdContext) {

        try {
            final ProgressDialog dialog = new ProgressDialog(pdContext);
            dialog.setCanceledOnTouchOutside(false);
            try {
                dialog.show();
                dialog.setContentView(R.layout.progress_layout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            } catch (WindowManager.BadTokenException | IllegalStateException e) {
                Logger.printStackTrace(e);
            }
            return dialog;
        } catch (NullPointerException e) {
            Logger.printStackTrace(e);
            return null;
        }

    }
    public interface NetworkResponse {
        void onSuccessResponse(String response);
        void onErrorResponse(VolleyError volleyError);
    }
}
