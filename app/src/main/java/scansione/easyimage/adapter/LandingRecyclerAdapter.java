package scansione.easyimage.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import scansione.easyimage.helper.Constant;
import scansione.easyimage.logger.Logger;
import scansione.easyimage.model_classes.BaseModel;
import scansione.easyimage.utilities.ImageCaching;
import scansione.easyimage.utilities.ImageDownloader;
import scansione.easyimage.viewholders.LandingRowHolder;
import scansione.mindvalley_ajijulmondal_android_test.R;
import scansione.easyimage.interfaces.SetOnItemClickListener;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class LandingRecyclerAdapter extends RecyclerView.Adapter<LandingRowHolder> {
    private final ImageDownloader mImageFetcher;
    private ArrayList<BaseModel> baseModels;
    private Context context;
    private SetOnItemClickListener listener;

    public LandingRecyclerAdapter(ArrayList<BaseModel> baseModels, AppCompatActivity activity,
                                  SetOnItemClickListener listener) {
        this.baseModels = baseModels;
        this.context = activity;
        ImageCaching.ImageCacheParams cacheParams =
                new ImageCaching.ImageCacheParams(activity, Constant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageDownloader takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageDownloader(context, 512);
        mImageFetcher.setLoadingImage(R.drawable.ic_default_144);
        mImageFetcher.addImageCache(activity.getSupportFragmentManager(), cacheParams);
        this.listener = listener;
    }

    @Override
    public LandingRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LandingRowHolder(LayoutInflater.from(context).inflate(R.layout.row_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final LandingRowHolder holder, final int position) {

        BaseModel baseModel = baseModels.get(position);
        String pathRequest = baseModel.getUser().getProfile_image().getLarge();
        holder.rowRecycler_imv.setImageDrawable (null);
        Logger.setLogTag("JIIII");
        Logger.showErrorLog("URL" + pathRequest);
//        Picasso.with(context).load(pathRequest).into(holder.rowRecycler_imv);
        mImageFetcher.loadImage(pathRequest, holder.rowRecycler_imv);
        holder.rowRecycler_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position,holder.rowRecycler_imv);
            }
        });

    }

    @Override
    public int getItemCount() {
        return baseModels.size();
    }

    public void notifyMyAdapter(ArrayList<BaseModel> baseModels) {
        this.baseModels = new ArrayList<>(baseModels);
        notifyDataSetChanged();
    }
}
