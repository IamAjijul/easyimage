package scansione.easyimage.interfaces;

import android.widget.ImageView;

/**
 * Created by AJIJUL on 12/4/2016.
 */

public interface SetOnItemClickListener {
    void onItemClick(int position, ImageView imageView);
}
