package scansione.easyimage.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import scansione.easyimage.helper.Constant;
import scansione.easyimage.utilities.ImageCaching;
import scansione.easyimage.utilities.ImageDownloader;
import scansione.mindvalley_ajijulmondal_android_test.R;
import scansione.easyimage.model_classes.BaseModel;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class DetailsActivity extends BaseActivity implements View.OnClickListener {
    private ImageDownloader mImageFetcher;
    private BaseModel baseModel;
    private TextView toolbar_tv, detailsActivity_tvFullName, detailsActivity_tvUserName, detailsActivity_tvLikes;
    private LinearLayout detailsActivity_llFooter;
    private ImageView activityDetails_imvProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        baseModel = getIntent().getParcelableExtra(Constant.EXTRA);
        initClassViewsAndVariables();
    }

    private void initClassViewsAndVariables() {
        toolbar_tv = (TextView) findViewById(R.id.toolbar_tv);
        toolbar_tv.setOnClickListener(this);
        detailsActivity_tvFullName = (TextView) findViewById(R.id.detailsActivity_tvFullName);
        detailsActivity_tvUserName = (TextView) findViewById(R.id.detailsActivity_tvUserName);
        detailsActivity_tvLikes = (TextView) findViewById(R.id.detailsActivity_tvLikes);
        detailsActivity_llFooter = (LinearLayout) findViewById(R.id.detailsActivity_llFooter);
        activityDetails_imvProfile = (ImageView) findViewById(R.id.activityDetails_imvProfile);
        if (baseModel != null) {

            ImageCaching.ImageCacheParams cacheParams =
                    new ImageCaching.ImageCacheParams(this, Constant.IMAGE_CACHE_DIR);

            cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

            // The ImageDownloader takes care of loading images into our ImageView children asynchronously
            mImageFetcher = new ImageDownloader(this, 100);
            mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
            detailsActivity_tvLikes.setText("" + baseModel.getLikes());
            detailsActivity_tvFullName.setText(baseModel.getUser().getName());
            detailsActivity_tvUserName.setText(baseModel.getUser().getUsername());
            detailsActivity_llFooter.setBackgroundColor(Color.parseColor(baseModel.getColor()));
            mImageFetcher.loadImage(baseModel.getUser().getProfile_image().getLarge(), activityDetails_imvProfile);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar_tv:
                ActivityCompat.finishAfterTransition(this);

                break;
        }
    }
}
