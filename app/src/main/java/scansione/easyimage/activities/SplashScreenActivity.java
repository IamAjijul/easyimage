package scansione.easyimage.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import scansione.mindvalley_ajijulmondal_android_test.R;

/**
 * Created by AJIJUL on 12/3/2016.
 */
public class SplashScreenActivity extends AppCompatActivity {

    private Runnable runnable;
    private long SPLASH_TIMEOUT = 3000;
    String TAG = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_activity);
        redirectToLandingActivity();


    }

    private void redirectToLandingActivity() {
        runnable = new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreenActivity.this, LandingActivity.class));
                finish();
            }
        };
        new Handler().postDelayed(runnable, SPLASH_TIMEOUT);
    }
}