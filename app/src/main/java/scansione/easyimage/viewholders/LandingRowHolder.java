package scansione.easyimage.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import scansione.mindvalley_ajijulmondal_android_test.R;

/**
 * This class used to hold view for recycler adapter
 * Created by AJIJUL on 12/3/2016.
 */

public class LandingRowHolder extends RecyclerView.ViewHolder {
    public ImageView rowRecycler_imv;


    public LandingRowHolder(View itemView) {
        super(itemView);

        rowRecycler_imv = (ImageView) itemView.findViewById(R.id.rowRecycler_imv);
    }
}
