package scansione.easyimage.model_classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class BaseModel implements Parcelable {
    String id,created_at,color;
    int width,height,likes;
    boolean liked_by_user;
    UserModel user;

    protected BaseModel(Parcel in) {
        id = in.readString();
        created_at = in.readString();
        color = in.readString();
        width = in.readInt();
        height = in.readInt();
        likes = in.readInt();
        liked_by_user = in.readByte() != 0;
        user = in.readParcelable(UserModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(created_at);
        dest.writeString(color);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeInt(likes);
        dest.writeByte((byte) (liked_by_user ? 1 : 0));
        dest.writeParcelable(user, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseModel> CREATOR = new Creator<BaseModel>() {
        @Override
        public BaseModel createFromParcel(Parcel in) {
            return new BaseModel(in);
        }

        @Override
        public BaseModel[] newArray(int size) {
            return new BaseModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
/*
}
{
        "id":"4kQA1aQK8-Y",
        "created_at":"2016-05-29T15:42:02-04:00",
        "width":2448,
        "height":1836,
        "color":"#060607",
        "likes":12,
        "liked_by_user":false,

        },*/
