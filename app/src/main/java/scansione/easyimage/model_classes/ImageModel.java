package scansione.easyimage.model_classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AJIJUL on 12/3/2016.
 */

public class ImageModel implements Parcelable {
    String small,medium,large;

    protected ImageModel(Parcel in) {
        small = in.readString();
        medium = in.readString();
        large = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(small);
        dest.writeString(medium);
        dest.writeString(large);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
/*"profile_image":{
        "small":"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\u0026q=80\u0026fm=jpg\u0026crop=faces\u0026fit=crop\u0026h=32\u0026w=32\u0026s=63f1d805cffccb834cf839c719d91702",
        "medium":"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\u0026q=80\u0026fm=jpg\u0026crop=faces\u0026fit=crop\u0026h=64\u0026w=64\u0026s=ef631d113179b3137f911a05fea56d23",
        "large":"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\u0026q=80\u0026fm=jpg\u0026crop=faces\u0026fit=crop\u0026h=128\u0026w=128\u0026s=622a88097cf6661f84cd8942d851d9a2"
        },
        "links":{
        "self":"https://api.unsplash.com/users/nicholaskampouris",
        "html":"http://unsplash.com/@nicholaskampouris",
        "photos":"https://api.unsplash.com/users/nicholaskampouris/photos",
        "likes":"https://api.unsplash.com/users/nicholaskampouris/likes"
*/